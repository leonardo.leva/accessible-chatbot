# Accessible Chat


## GIT

### Branches

- _master_: production codebase;
- _develop_: development codebase.


### Commit Style

_.gitmessage_

Use `git config --local commit.template .gitmessage`.


## Setup

Run `npm ci`.


## Run

Run `npm run sass-watch`.

Run `npm run replace-scripts`.


## Build

Run `npm run build`.


## Requirements

- node >= 12.4.0
- npm >= 6.9.0


## Technologies

- HTML
- Javascript
- SASS
