Acchat.qrView = (function(){
 
  var Quick_Response = function(tips, getId){
    var chips = tips.chips;
    var qr = document.createElement('div');
    qr.setAttribute('class','quick-response');      
    var ul = document.createElement('ul');

    for (var i = 0; i < chips.length; i++) {        
      var chip = chips[i];
      var li = document.createElement('li');
      var btn = document.createElement('button');
      btn.appendChild(document.createTextNode(chip.content));
      li.appendChild(btn);
      ul.appendChild(li);
      
      btn.chipData = {id: chip.id, content: chip.content};

      btn.addEventListener('click', function(){
       getId(this.chipData); 
      });
    }
    
    qr.appendChild(ul);    

    return qr;
      
  };
 
  return Quick_Response;
   
})();
