Acchat.chatView = (function() {
  return function(options) {

    var margin = 10;
    var border = 2;
    var w = window.innerWidth - (2*margin + 2*border); 
    var h = window.innerHeight - (2*margin + 2*border);
   
    var jsChat = document.createElement('div');
    jsChat.setAttribute('class','chat-box');
    jsChat.setAttribute('role', 'region');
    jsChat.setAttribute('aria-label', 'chatbot');
    jsChat.setAttribute('tabindex', '-1');

    jsChat.style.border = border + 'px solid #4137ef';
    jsChat.style.fontSize = '12px';
    jsChat.style.backgroundColor = '#fff';

    var header = document.createElement('header');
    header.setAttribute('class','chat-box__header');

    var logo = document.createElement('img');
    logo.setAttribute('src', '../src/assets/img/Logo.svg');
    logo.setAttribute('alt','fitbot');

    var closeBtn = document.createElement('button');
    closeBtn.setAttribute('class', 'chat-box__header__close--btn');
    closeBtn.setAttribute('aria-label', 'chiudi chat');

    header.appendChild(logo);
    header.appendChild(closeBtn);
    jsChat.appendChild(header);

   closeBtn.addEventListener('click', function(){
     options.parent.removeChild(jsChat);
     options.parent.appendChild(openBtn);
      
    });

    var skipLink = document.createElement('div');
    skipLink.setAttribute('class', 'skiplink');
    skipLink.setAttribute('tabindex', '-1');
    
    var skipBtn = document.createElement('button');
    skipBtn.appendChild(document.createTextNode('Apri Chatbot'));

    skipLink.appendChild(skipBtn);
    options.parent.appendChild(skipLink);


    var openBtn = document.createElement('button');
    openBtn.setAttribute('class', 'initBtn');
    openBtn.setAttribute('aria-label', 'parla con fitbot');

    options.parent.appendChild(openBtn);

    function openChat(){

      options.getIntro({id:'INTRO'});

      if(h > w) { //if it's in portrait mode (like on phone) open in full width
        jsChat.style.width = w + 'px';
        jsChat.style.height = h + 'px';
        jsChat.style.margin =  margin + 'px';
        jsChat.style.position = 'absolute';
        jsChat.style.top = '0';
        jsChat.style.left = '0';        
      } 
      
      else { //if it's not on portrait mode open in a box
        jsChat.style.width = '280px';
        jsChat.style.height = '500px';
        jsChat.style.position = 'fixed';
        jsChat.style.right = '50px';
        jsChat.style.bottom = '50px';                
      } 
        
      options.parent.appendChild(jsChat);
      options.parent.removeChild(openBtn);
      skipBtn.blur();
      /*
      window.setTimeout(function(){
        document.getElementById('in').focus();
      }, 300); 
      */
    }; // openChat

    openBtn.addEventListener('click', openChat);
    skipBtn.addEventListener('click', openChat);

    this.getContainer = function() {
      return jsChat;
    };
    return this;
  };
})();
