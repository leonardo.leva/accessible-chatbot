Acchat.Counter = (function(){
 
  var Counter = function(attributes){ 
    var hits = attributes.hits; 
    var series = attributes.series;
    var actualSeries = attributes.actualSeries;    
    var hitDuration = 1000 * attributes.speed;
    var ordinal;
    switch (actualSeries){
     case 1: {ordinal = 'prima'; break}
     case 2: {ordinal = 'seconda'; break}
     case 3: {ordinal = 'terza'; break}
     case 4: {ordinal = 'quarta'; break}
     case 5: {ordinal = 'quinta';} 
    }; 
      
    var ariaAid = 'esegui ' + hits + ' colpi in circa '+ (attributes.speed*hits) + ' secondi, questa è la ' + ordinal + ' di '+ series+ ' ripetizioni';

    var counter = document.createElement('section');
    counter.setAttribute('aria-label', ariaAid);
    counter.setAttribute('class','botcounter'); 
    counter.setAttribute('tabindex','0');     

    var count = document.createElement('h1');
    count.setAttribute('aria-hidden','true');
    count.appendChild(document.createTextNode('0' + '/' + hits));

    var bar = document.createElement('div');
    bar.setAttribute('class', 'botcounter--bar');

    var progress = document.createElement('div');
    progress.setAttribute('class', 'botcounter--progress');

    bar.appendChild(progress);

    var par = document.createElement('p');
    par.setAttribute('aria-hidden','true')
    par.appendChild(document.createTextNode(actualSeries + '/' + series));
    par.appendChild(document.createElement('br'));
    par.appendChild(document.createTextNode('Ripetizioni'));
       
    counter.appendChild(count);
    counter.appendChild(bar); 
    counter.appendChild(par);
        
    for(var i = 1; i <= hits; i++){
      (function(i){  
        window.setTimeout(function() {
        count.textContent = i + '/' + hits;
        progress.style.width = (100/hits)*i + '%';
        }, hitDuration*i);
      })(i);  
    }   

    return counter;
      
  };

 
  return Counter;
  
 
})();
