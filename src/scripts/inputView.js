Acchat.inputView = (function() {
  return function(options) {
    var sbContainer = document.createElement('div');
    sbContainer.setAttribute('class', 'chat-box__sendbox__container');
    options.parent.appendChild(sbContainer);

    var sendBox = document.createElement('div');
    sendBox.setAttribute('class', 'chat-box__sendbox');
    sbContainer.appendChild(sendBox);

    var sendForm = document.createElement('form');
    sendForm.setAttribute('id','frm');
    sendForm.setAttribute('action', '');
    sendForm.setAttribute('class', 'chat-box__sendbox__form');

    sendForm.addEventListener('submit', function(event) {
      event.preventDefault();
      var message = sendForm.elements.namedItem('in').value
      if (message !== '' && !/^\s*$/.test(message)){ // prevent sending empty messages
        options.onSendMessage(message);
        input.disabled = true;
      }
    });
    sendBox.appendChild(sendForm);

    var input = document.createElement('input');
    input.setAttribute('id', 'in');
    input.setAttribute('type', 'text');
    input.setAttribute('placeholder', 'Scrivi...');
    input.setAttribute('autocomplete', 'off');
    sendForm.appendChild(input);

    var btn = document.createElement('button');
    btn.setAttribute('type', 'submit');
    btn.setAttribute('aria-label', 'invia');
    sendForm.appendChild(btn);

    this.reset = function() {
      sendForm.reset();
      input.disabled = false;
    };

  };
})();
