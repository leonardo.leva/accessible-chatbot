Acchat.Card = (function(){

  var Card = function(content){

    var card = document.createElement('div');
    card.setAttribute('class', 'botcard');
    card.setAttribute('tabindex', '0');
    card.setAttribute('aria-label', content.name);


    var img = document.createElement('img');
    img.setAttribute('src', content.url);
    img.setAttribute('alt', content.ariaDescription);

    var h1 = document.createElement('h1');
    h1.appendChild(document.createTextNode(content.name));

    var p = document.createElement('p');
    p.setAttribute('aria-hidden', 'true');
    p.appendChild(document.createTextNode(content.description));

    card.appendChild(img);
    card.appendChild(h1);
    card.appendChild(p);

    return card;

  }

  return Card

})();