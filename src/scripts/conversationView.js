Acchat.conversationView = (function() {
  
  return function(options) {
    
    var conversation = document.createElement('div');
    conversation.setAttribute('class', 'chat-box__conversation');
    conversation.setAttribute('aria-live', 'polite');
    options.parent.appendChild(conversation);

    var dots = document.createElement('img');
    dots.setAttribute('src', '../src/assets/gifs/loading_dots.gif');
    dots.setAttribute('alt', '');

    var dotsContainer = document.createElement('div');
    dotsContainer.setAttribute('class', 'dots');
    dotsContainer.appendChild(dots);

    this.isTyping = function(){
      window.setTimeout(function(){
        conversation.appendChild(dotsContainer);
        dotsContainer.scrollIntoView({ block: 'end', behavior: "smooth"});
      }, 300);
    }

    this.stopTyping = function(){
      if(conversation.lastChild === dotsContainer )
      conversation.removeChild(dotsContainer);
    };

    this.renderMessage = function(message) {
      var bubble = new Acchat.bubbleView(message);
      var last = conversation.lastChild;
     
      if(last !== null && last.className == 'quick-response') { //delete chips when selected
        conversation.removeChild(last);
      };

      if(last!== null){ //delete timer or counter when skipped
        var secondLast = last.previousSibling;
        if(secondLast !== null && last.className !== 'sr-only'){
           if( secondLast.className == 'botcounter' || secondLast.className == 'bottimer'){
              conversation.removeChild(secondLast);
           };
        };
      };
      
      conversation.appendChild(bubble);
      bubble.scrollIntoView({ block: 'end', behavior: "smooth"});
    };
  
    this.renderQR = function(qr){ 
      var chips = new Acchat.qrView(qr, options.getId);
      conversation.appendChild(chips);
      chips.scrollIntoView({ block: 'end', behavior: "smooth"});
    };

    this.renderTimer = function(time){
      var timer = new Acchat.Timer(time);
      conversation.appendChild(timer);
      timer.scrollIntoView({ block: 'end', behavior: "smooth"});     
    };
    
    this.renderCounter = function(attributes){
      var counter = new Acchat.Counter(attributes,  options.lastKey);
      conversation.appendChild(counter);
      counter.scrollIntoView({ block: 'end', behavior: "smooth"});     
    };

    this.renderList = function(items){
      var list = new Acchat.List(items, options.getId);
      conversation.appendChild(list);
      list.scrollIntoView({ block: 'end', behavior: "smooth"});     
    };

    this.renderCard = function(content){
      var card = new Acchat.Card(content);
      conversation.appendChild(card);
      card.scrollIntoView({block: 'end', behavior: "smooth", inline:"nearest"});
    };

    this.renderCarousel = function(content){
      var carousel = new Acchat.Carousel(content, options.getId);
      conversation.appendChild(carousel);
      carousel.scrollIntoView({ block: 'end', behavior: "smooth"});     
    };

    this.renderTimerManager = function(settings){
      var timerManager = new Acchat.TimerManager(settings, options.getId);
      var last = conversation.lastChild;
      if(last !== null && last.className == 'quick-response') { //delete chips above
        conversation.removeChild(last);
      };
      conversation.appendChild(timerManager);

    }
    
  };

})();
