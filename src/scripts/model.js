Acchat.conversationModel = (function() {

  var Message = function(options) {
    this.text = options.text;
    this.sender = options.sender;
    this.type = options.type;
    this.isLast = options.isLast;
  };

  var QR = function (options) {
    this.chips = options.chips;
    this.type = options.type;

  }

  var Timer = function (options) {
    this.time = options.time;
    this.type = options.type;
    this.isLast = options.isLast;
  }

  var Counter = function (options) {
    this.hits = options.hits;
    this.series = options.series;
    this.actualSeries = options.actualSeries;
    this.type = options.type;
    this.isLast = options.isLast;
    this.speed = options.speed;
  }

  var ManageTimer = function (options){
    this.id = options.id;
    this.time = options.time;
    this.ariaAid = options.ariaAid;
    this.type = options.type;
    this.myKey = options.myKey;
  }

  var List = function (options) {
    this.items = options.items;
    this.name = options.name;
    this.type = options.type;
    this.isLast = options.isLast;
  }

  var Card = function(options) {
    this.url = options.url;
    this.name = options.name;
    this.description = options.description;
    this.ariaDescription = options.ariaDescription;
    this.type = options.type;
    this.isLast = options.isLast;
  }

  var Carousel = function(options) {
    this.elements = options.elements;
    this.type = options.type;
    this.isLast = options.isLast;
  }

  return function(options) {
     
   // Array of messages (sent and received) 
    var conversation = [];

   // Last id called from json
   var lastCall = [];

   // Url of mocked conversation 
    var jsonUrl = 'https://api.jsonbin.io/b/5db00e0dbf41f13c32511871/6';

    //get the json file with the server's answers    
    function getJson(){
    var request = new XMLHttpRequest();
      request.open('GET', jsonUrl);    
      request.responseType = 'json';
      request.send();
      return request;
    }

    function buildResponse(branch){
              
      for(var i = 0; i < branch.length; i++){
       (function(i){  
          window.setTimeout(function() {
                
            singleObject =  branch[i];

            switch (singleObject.type){

              case 'simple':{
                if(singleObject.sender == undefined){
                  singleObject.sender = 'bot';
                }
                var sentMessage = new Message(singleObject);
                conversation.push(sentMessage);
                options.onMessageReceived(sentMessage);
              } break;

              case 'chips':{
                var sentMessage = new QR(singleObject);
                conversation.push(sentMessage);
                options.onMessageReceived(sentMessage);
              } break;

              case 'list':{
                var sentMessage = new List(singleObject);
                conversation.push(sentMessage);
                options.onMessageReceived(sentMessage);
              } break;

              case 'card':{
                var sentMessage = new Card(singleObject);
                conversation.push(sentMessage);
                options.onMessageReceived(sentMessage);
              } break;

              case 'timer':{
                var sentMessage = new Timer(singleObject);
                conversation.push(sentMessage);
                options.onMessageReceived(sentMessage);
              } break;

              case 'counter':{
                var sentMessage = new Counter(singleObject);
                conversation.push(sentMessage);
                options.onMessageReceived(sentMessage);
              } break;

              case 'carousel':{
                var sentMessage = new Carousel(singleObject);
                conversation.push(sentMessage);
                options.onMessageReceived(sentMessage);
              } break;
                         
              case 'timerManager' : {
                var sentMessage = new ManageTimer(singleObject);
                conversation.push(sentMessage);
                options.onMessageReceived(sentMessage);                
              }break;

            }
          }, 1500*i); //close timeout inside for
        })(i);  //close IIFE for timeout
      } // close for loop
    } // close buildResponse

    if (options.messages !== undefined) {
      conversation.concat(options.messages);
    }

    // PICK RESPONSE FROM RETURNED ID

    this.selectedId = function(idObj){

      var request = getJson();
      request.onload = function() { 
        var data = request.response;

        switch (idObj.id){
        
          case 'INTRO':
           if (conversation.length == 0){
             buildResponse(data.INTRO);
           };
          break;
      
          default:{
            var renderMe = data[idObj.id];
            if (idObj.content !== undefined) { //render clicked selection (chips, carousel element or list item)
              var addMe = {type:'simple', text: idObj.content, sender: 'user', isLast:false}; //render user selection as bubble
              renderMe.unshift(addMe);
            }
           if (lastCall[lastCall.length - 1] !== idObj.id) { //disable double selection
             lastCall.push(idObj.id);
             buildResponse(renderMe);
           }

          }
        }
        
      }

    }
    
    //Send a message to the server and store it in conversation
    this.sendMessage = function(message, onMessageSent) {
     
      //Render typed response
      window.setTimeout(function() { 
        var sentMessage = new Message({text : message, sender: 'user', type: "simple"});
        conversation.push(sentMessage);
        onMessageSent(sentMessage);

        // Mocked Server
        // Receive a message        
        window.setTimeout(function() {

          // WELCOME MESSAGE

          if(conversation.length === 1){ 

            var request = getJson();
                  
            request.onload = function() { 
              var data = request.response;
              buildResponse(data.INTRO);
              
            } //request.onload
                  
          }
          
          //FALLBACK MANAGEMENT and match typed with clickable
          
          else{ // return fallback or switch 'message' as string            

            //look for last chip sent
            var lastClickable;
            var id;
            var checker = conversation.length-1;
            

            while (lastClickable == undefined){ //seek the last clickable object (list, chips or carousel)
              if(conversation[checker].type=="chips" || conversation[checker].type=="list" || conversation[checker].type=="carousel"){
                lastClickable = conversation[checker];
              }
              checker--;
            }

            var isTheSame = false; // boolean for matching typed with clickable

            switch(lastClickable.type){

              case 'chips' : {
                for(var i = 0; i<lastClickable.chips.length; i++){
                  if(message.toLowerCase() == lastClickable.chips[i].content.toLowerCase()){
                    isTheSame = true;
                    id = lastClickable.chips[i].id;                                
                  }
                }
              } break;

              case 'list' : {
                for(var i = 0; i<lastClickable.items.length; i++){
                  if(message.toLowerCase() == lastClickable.items[i].ex_name.toLowerCase()){
                    isTheSame = true;
                    id = lastClickable.items[i].id;                                
                  }
                }
              }break;
              case 'carousel' :{
                for(var i = 0; i<lastClickable.elements.length; i++){
                  if(message.toLowerCase() == lastClickable.elements[i].name.toLowerCase()){
                    isTheSame = true;
                    id = lastClickable.elements[i].id;                                
                  }
                }
              }
            }

            var request = getJson();
                  
            request.onload = function() { 

              if (isTheSame) {
                var data = request.response;
                lastCall.push(id);
                buildResponse(data[id]);
              }
              
              else {
                var fallback = request.response.FALLBACK;
                fallback.push(lastClickable);
                buildResponse(fallback);
              }

            }
          } 
          
        }, 1000); 

      }, 500); // delayed input render

    }; // sendMessage

    /**
     * Get all messages
     *
     * @returns {Array} All messages
     */
    this.getMessages = function() {
      return conversation;
    };

    this.getLastCall = function() {
      return lastCall;
    } 
    
    return this;

  };
})();
