Acchat.Timer = (function(){
 
  var Timer = function(duration){ //duration must be a number
    var fullTime = duration.time;
    var str = 'riposa per '+ fullTime + ' secondi';
    var timer = document.createElement('section');
    timer.setAttribute('class','bottimer');      
    timer.setAttribute('tabindex','0');
    timer.setAttribute('aria-label',str);            

    var par = document.createElement('p');
    par.setAttribute('aria-hidden','true');
    par.appendChild(document.createTextNode('Recupero'));
   
    var time = document.createElement('h1');
    time.setAttribute('aria-hidden','true');
    var now = fullTime;
    time.appendChild(document.createTextNode(now + 's'));

    timer.appendChild(par);
    timer.appendChild(time); 
    
    for(var i = fullTime; i > 0; i--){
      (function(i){  
        window.setTimeout(function() {
          time.textContent = --now + 's';
        }, 1000*i);
      })(i);
    } 

    return timer;    
  };

  return Timer;
})();
