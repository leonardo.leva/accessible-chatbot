Acchat.TimerManager = (function(){
 
	var TimerManager = function(settings, getId){ 
    var aria = settings.ariaAid;
    getId({id:settings.id});

	  var timerManager = document.createElement('div');
	  timerManager.setAttribute('class','sr-only');      
	  timerManager.setAttribute('tabindex','-1');      
  
	  var par = document.createElement('p');
	  par.appendChild(document.createTextNode(aria));

	  timerManager.appendChild(par);
  
	  return timerManager;    
	};
  
	return TimerManager;
  })();
  