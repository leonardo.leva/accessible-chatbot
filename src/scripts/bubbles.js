Acchat.bubbleView = (function(){  
  var Bubble = function(message) {
    var bubble = document.createElement('section');
    bubble.setAttribute('tabindex', '0')

    if (message.sender === 'bot') {
      bubble.setAttribute('class', 'message message--bot');
      bubble.setAttribute('aria-label', 'il bot dice');
    } else {
      bubble.setAttribute('class', 'message message--user');
      bubble.setAttribute('aria-label', 'tu dici');
    }

    var par = document.createElement('p');
    par.appendChild(document.createTextNode(message.text));
    bubble.appendChild(par);

    return bubble;
  };

  return Bubble;
})();
