Acchat.List = (function(){

  var List = function(elements, getId){
    var items = elements.items;
    var listName = elements.name;

    var list = document.createElement('div');
    list.setAttribute('class', 'botlist');
    list.setAttribute('tabindex', '0');

    var title = document.createElement('h1');
    title.appendChild(document.createTextNode(listName));

    var ul = document.createElement('ul');

    for(var i=0; i<items.length; i++){
      var item = items[i];
      var li = document.createElement('li');      
      var p = document.createElement('p');

      p.setAttribute('aria-hidden', 'true');
      p.appendChild(document.createTextNode(item.ex_name + ' ' + item.hits + 'x'+ item.rep));

      var btn = document.createElement('button');
      btn.setAttribute('aria-label', ('info ' + item.ex_name));

      btn.listData = {id: item.id, content: 'Info ' + item.ex_name};

      btn.addEventListener('click', function(){
        getId(this.listData);
        this.blur();
      });
      
      li.appendChild(p);
      li.appendChild(btn);
      ul.appendChild(li);      
      
    }

    list.appendChild(title);
    list.appendChild(ul);

    return list;

  }

  return List;


})();