Acchat.Carousel = (function(){
    
    var Carousel = function(objects, getId){
        
        var elements = objects.elements; //array of cards objects

        var carousel = document.createElement('div');
        carousel.setAttribute('class','botcarousel');

        var wrapper = document.createElement('div');
        wrapper.setAttribute('class','botcarousel--carousel__container');

        for(var i = 0; i < elements.length; i++){

            var element = elements[i];

            var btn = document.createElement('button');
            btn.name = element.name;

            var car_el = document.createElement('div');
            car_el.setAttribute('class', 'botcarousel--car__el');
            car_el.setAttribute('tabindex', '-1');
            
            var img = document.createElement('img');
            img.setAttribute('src', element.url);
            img.setAttribute('alt', ' ');

            var h1 = document.createElement('h1');
            h1.appendChild(document.createTextNode(element.name));

            var p =  document.createElement('p');
            p.appendChild(document.createTextNode(element.description));

            car_el.appendChild(img);
            car_el.appendChild(h1);
            car_el.appendChild(p);

            btn.carouselData = {id: element.id, content: element.name};

            btn.addEventListener('click', function(){
                getId(this.carouselData);
                this.blur();
                this.firstChild.blur();
            });
            

            btn.appendChild(car_el);
            wrapper.appendChild(btn);

        }

        var prev_btn = document.createElement('button');
        prev_btn.setAttribute('class', 'botcarousel--car__arr__left');
        prev_btn.setAttribute('tabindex', '-1');
        prev_btn.addEventListener('click', function(){
            wrapper.scrollBy(-228,0);
        });
        
        var next_btn = document.createElement('button');
        next_btn.setAttribute('class', 'botcarousel--car__arr__right');
        next_btn.setAttribute('tabindex', '-1');
        next_btn.addEventListener('click', function(){
            wrapper.scrollBy(228,0);
        });

        carousel.appendChild(wrapper);
        carousel.appendChild(prev_btn);
        carousel.appendChild(next_btn);

        return carousel;

    }

    return Carousel;
})(); 

