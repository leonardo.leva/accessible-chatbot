var Acchat = (function() {
  return function(parent){ 

    var sendMessage = function(message) {
      conversationModel.sendMessage(message, function(sentMessage) {
        conversationView.renderMessage(sentMessage);
        inputView.reset();
        conversationView.isTyping();
      });
    };

    var selectedId = function(idObj){
      conversationModel.selectedId(idObj);
    }

    var chatView = new Acchat.chatView({
      parent: parent,
      getIntro: selectedId,
    });

    var conversationView = new Acchat.conversationView({
      parent: chatView.getContainer(), 
      sendMessage: sendMessage,
      getId : selectedId,
    });

    var inputView = new Acchat.inputView({
      parent: chatView.getContainer(),
      onSendMessage: sendMessage,
    });

    var conversationModel = new Acchat.conversationModel({
      //test simple messages
      
      onMessageReceived: function(message) {
        switch (message.type) {

          case 'simple' : {
            conversationView.stopTyping();
            conversationView.renderMessage(message);
            if(!message.isLast){
              conversationView.isTyping();
            }
          } break;

          case 'chips' : {
            conversationView.stopTyping();
            conversationView.renderQR(message);
          } break;

          case 'list' : {
            conversationView.stopTyping();
            conversationView.renderList(message);
            if(!message.isLast){
              conversationView.isTyping();
            }
          } break;

          case 'card' : {
            conversationView.stopTyping();
            conversationView.renderCard(message);
            if(!message.isLast){
              conversationView.isTyping();
            }
          } break;

          case 'timer' : {
            conversationView.stopTyping();
            conversationView.renderTimer(message);
            if(!message.isLast){
              conversationView.isTyping();
            }
          } break;

          case 'counter' : {
            conversationView.stopTyping();
            conversationView.renderCounter(message);
            if(!message.isLast){
              conversationView.isTyping();
            }
          } break;

          case 'carousel' :{
            conversationView.stopTyping();
            conversationView.renderCarousel(message);
            if(!message.isLast){
              conversationView.isTyping();
            }
          }break;

          case 'timerManager' :{
            time = message.time * 1000;
            window.setTimeout(function(){ 
              if (lastCall[lastCall.length-1] == message.myKey){ //render timerManager only if the timer/counter it's not skipped
                conversationView.renderTimerManager(message)
              }
            }, time);
           
          }

        }; // switch    
        
        //document.getElementById('in').focus(); // select the input field automatically
                     
      }, //onMessageReceived

    }); //conversationModel

    // Get the array of json key called (id)
    var lastCall = conversationModel.getLastCall();
        
    return this;

  };

})();
